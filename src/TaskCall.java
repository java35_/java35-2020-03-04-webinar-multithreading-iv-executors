import java.util.concurrent.Callable;

public class TaskCall implements Callable<Integer> {
	int[] column;
	
	TaskCall(int[] column) {
		this.column = column;
	}
	
	@Override
	public Integer call() {
		int sum = 0;
		for (int value : column) {
			sum = sum + value;
		}
		return sum;
	}

}
