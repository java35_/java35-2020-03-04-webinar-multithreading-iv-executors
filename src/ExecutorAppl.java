import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ExecutorAppl {
	static final int SIZE = 100;
	public static void main(String[] args) throws InterruptedException, ExecutionException {
//		ExecutorService exec = Executors.newCachedThreadPool();
		
		int[][] array2x = new int[SIZE][];
		
		for (int i = 0; i < array2x.length; i++) {
			int[] array1x = new int[SIZE];
			for (int j = 0; j < array1x.length; j++) {
				array1x[j] = j + 1;
			}
			array2x[i] = array1x;
		}
		
		ExecutorService exec = 
				Executors.newFixedThreadPool(4);
		
		
		
//		exec.execute(() -> System.out.println("1"));
		
//		Task[] tasks = new Task[SIZE];
//		for (int i = 0; i < SIZE; i++) {
//			tasks[i] = new Task(array2x[i]);
//		}
//		
//		for (Task task : tasks) {
//			exec.execute(task);
//		}
		
		TaskCall[] tasks = new TaskCall[SIZE];
		for (int i = 0; i < SIZE; i++) {
			tasks[i] = new TaskCall(array2x[i]);
		}
		
		Future<Integer>[] columnSums = new Future[SIZE];
		for (int i = 0; i < SIZE; i++) {
			columnSums[i] = exec.submit(tasks[i]);
		}
		exec.shutdown();
		
		int sum = 0;
		for (int i = 0; i < SIZE; i++) {
			sum = sum + columnSums[i].get();
		}
		
//		exec.shutdownNow();
//		exec.awaitTermination(2, TimeUnit.SECONDS);
		
//		Thread.sleep(1000);
		
		System.out.println(sum);
	}

}