
class Task implements Runnable {
	int[] column;
	public static int sumOfTable;
	Object mutex = new Object();
	
	Task(int[] column) {
		this.column = column;
	}
	
	@Override
	public void run() {
		int sum = 0;
//		try {
//			Thread.sleep(100);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		for (int value : column) {
			sum = sum + value;
		}
		synchronized (mutex) {
			sumOfTable = sumOfTable + sum;
		}
	}
}
