
public class Appl {

	public static void main(String[] args) {
		int[] array1x_1 = new int[5];
		
		array1x_1[0] = 1;
		array1x_1[1] = 2;
		array1x_1[2] = 3;
		array1x_1[3] = 4;
		array1x_1[4] = 5;
		
		int[] array1x_2 = new int[3];
		
		array1x_2[0] = 10;
		array1x_2[1] = 20;
		array1x_2[2] = 30;
		
		
		int[][] array2x = new int[2][];
		
		array2x[0] = array1x_1;
		array2x[1] = array1x_2;
		
		System.out.println("Hello");
		
	}
	
	static void foo(int ...numbers) {
		for (int i : numbers) {
			System.out.println(i);
		}
	}
	

}
