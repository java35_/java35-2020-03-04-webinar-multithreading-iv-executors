
public class Appl2 {
	public static void main(String[] args) {
		int[][] array2x = new int[100][];
		
		for (int i = 0; i < array2x.length; i++) {
			int[] array1x = new int[100];
			for (int j = 0; j < array1x.length; j++) {
				array1x[j] = j + 1;
			}
			array2x[i] = array1x;
		}
		
		
//		int sum = 0;
//		for (int[] array1x : array2x) {
//			for (int num : array1x) {
//				sum = sum + num;
//			}
//		}
		
		int sum = 0;
		for (int[] array1x : array2x) {
			int columnSum = 0;
			for (int num : array1x) {
				columnSum = columnSum + num;
			}
			System.out.println(columnSum);
			sum = sum + columnSum;
		}
		
		System.out.println(sum);
	}
}
